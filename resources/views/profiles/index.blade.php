@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard </div>
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    <table class="table">
  <thead class="thead-dark">
    <tr>
      <th scope="col">id</th>
      <th scope="col">user_id</th>    
      <th scope="col">facebook</th>
      <th scope="col">twitter</th>
      <th scope="col">github</th>
      <th scope="col">Action</th>

    </tr>
  </thead>
  <tbody>
  @foreach($profiles as $profile)
    <tr>
    <td scope="col">{{$profile->id}}</td>
      <td scope="col"></td>
     
      <td scope="col">{{$profile->facebook}}</td>
      <td scope="col">{{$profile->twitter}}</td>
      <td scope="col">{{$profile->github}}</td>
      <td scope="col">Edit</td>
    </tr>
   @endforeach 
  </tbody>
</table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
